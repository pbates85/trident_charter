package com.trident.trident_charter.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController("api/v1/")
public class userController {

    @Qualifier("userServiceImpl")
    @Autowired
    private UserService userservice;

    @GetMapping("/user/{email}/{password}")
    public int UserLogin(@PathVariable("email") String email, @PathVariable("password") String password){
        int flag = userservice.loginValidation(email, password);

        if(flag == 0){
            System.out.println("Not Found");
            return 0;
        }
        else{
            System.out.println("Found");
            return flag;
        }
    }
}
