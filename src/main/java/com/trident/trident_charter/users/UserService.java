package com.trident.trident_charter.users;


import org.springframework.stereotype.Repository;

@Repository
public interface UserService {

    // return an int 1 based on correct email and pw else int 0
    public int loginValidation(String email, String password);
}
