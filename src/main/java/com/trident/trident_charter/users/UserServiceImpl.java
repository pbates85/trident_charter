package com.trident.trident_charter.users;

import com.trident.trident_charter.dbConnection.DButil;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Service
public class UserServiceImpl implements UserService{

    int flag = 0;

    Connection connection;

    public UserServiceImpl() throws SQLException {
        connection = DButil.getConnection();
    }

    @Override
    public int loginValidation(String email, String password) {
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE email = '"+email+"'");
            ResultSet rs = statement.executeQuery();

            while(rs.next()){
                if(rs.getString(4).equals(email) && rs.getString(5).equals(password)) {
                    flag = 1;
                }else{
                    System.out.println("Invalid Email or Password");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        // if user is found flag = 1
        return flag;
    }
}
