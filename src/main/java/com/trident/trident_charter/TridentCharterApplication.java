package com.trident.trident_charter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TridentCharterApplication {

    public static void main(String[] args) {
        SpringApplication.run(TridentCharterApplication.class, args);
    }

}
